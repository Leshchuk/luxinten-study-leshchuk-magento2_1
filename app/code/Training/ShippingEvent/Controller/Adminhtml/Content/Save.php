<?php
namespace Training\ShippingEvent\Controller\Adminhtml\Content;

use Magento\Backend\App\Action\Context;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Training_ShippingEvent::items';

    protected $shippingEventModelFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Training\ShippingEvent\Model\EventFactory $shippingEventModelFactory
    ) {
        $this->shippingEventModelFactory = $shippingEventModelFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $eventId = $this->getRequest()->getParam('event_id');

            if (empty($data['event_id'])) {
                $data['event_id'] = null;
            }

            $model = $this->shippingEventModelFactory->create()->load($eventId);
            if (!$model->getId() && $eventId) {
                $this->messageManager->addErrorMessage(__('This item no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the item successfully.'));

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['event_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving item.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['event_id' => $eventId]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
