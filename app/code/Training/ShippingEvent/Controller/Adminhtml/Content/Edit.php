<?php
namespace Training\ShippingEvent\Controller\Adminhtml\Content;

use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Training_ShippingEvent::items';

    protected $resultPageFactory;

    protected $shippingEventModelFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Training\ShippingEvent\Model\EventFactory $shippingEventModelFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->shippingEventModelFactory = $shippingEventModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('event_id');
        $model = $this->shippingEventModelFactory->create();

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This item no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Training_ShippingEvent::items');

        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New Item'));

        return $resultPage;
    }
}
