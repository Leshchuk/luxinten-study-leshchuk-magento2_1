<?php
namespace Training\ShippingEvent\Controller\Adminhtml\Content;

class NewAction extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Training_ShippingEvent::items';

    protected $resultForwardFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
