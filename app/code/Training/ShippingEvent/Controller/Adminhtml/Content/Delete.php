<?php
namespace Training\ShippingEvent\Controller\Adminhtml\Content;

use Magento\Backend\App\Action\Context;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Training_ShippingEvent::items';

    protected $resultPageFactory;

    protected $shippingEventModelFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Training\ShippingEvent\Model\EventFactory $shippingEventModelFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->shippingEventModelFactory = $shippingEventModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('event_id');

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->shippingEventModelFactory->create()->load($id);
                $model->delete();

                $this->messageManager->addSuccessMessage(__('The item has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['event_id' => $id]);
            }
        }

        $this->messageManager->addErrorMessage(__('We can\'t find item with such id.'));
        return $resultRedirect->setPath('*/*/');
    }
}
