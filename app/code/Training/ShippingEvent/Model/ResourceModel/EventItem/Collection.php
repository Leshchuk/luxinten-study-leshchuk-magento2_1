<?php
namespace Magento\Cms\Model\ResourceModel\Block;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * CMS Block Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'item_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'shipping_event_item_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'shipping_event_item_collection';


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\ShippingEvent\Model\EventItem::class, \Training\ShippingEvent\Model\ResourceModel\EventItem::class);
    }

}
