<?php
namespace Training\ShippingEvent\Model\ResourceModel;

use Training\ShippingEvent\Api\Data\EventItemInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\EntityManager\MetadataPool;
use \Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\EntityManager\EntityManager;

/**
 * CMS block model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EventItem extends AbstractDb
{
    protected $metadataPool;
    protected $entityManager;

    /**
     * Event constructor.
     * @param Context $context
     * @param EntityManager $entityManager
     * @param MetadataPool $metadataPool
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
)
    {
        $this->metadataPool     = $metadataPool;
        $this->entityManager    = $entityManager;
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('shipping_event_item', 'item_id');
    }

    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        return $this->metadataPool->getMetadata(EventItemInterface::class)->getEntityConnection();
    }



}
