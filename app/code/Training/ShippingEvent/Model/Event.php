<?php

namespace Training\ShippingEvent\Model;


use Training\ShippingEvent\Api\Data\EventInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;


class Event extends AbstractModel implements EventInterface, IdentityInterface
{
    /**
     * CMS block cache tag
     */
    const CACHE_TAG = 'shipping_event';

    /**#@+
     * Event's statuses
     */
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 0;


    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'shipping_event';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Training\ShippingEvent\Model\ResourceModel\Event::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG];
    }

    /**
     * Retrieve event id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::EVENT_ID);
    }

    /**
     * Retrieve Base Total Amount
     *
     * @return int
     */
    public function getBaseTotalAmount()
    {
        return $this->getData(self::TOTAL_AMOUNT);
    }

    /**
     * Retrieve event comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->getData(self::COMMENT);
    }

    /**
     * Retrieve event created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Retrieve event updated at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Retrieve Order ID
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * Retrieve Event Visibility
     *
     * @return int
     */
    public function getVisibility()
    {
        return $this->getData(self::VISIBILITY);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return EventInterface
     */
    public function setId($id)
    {
        return $this->setData(self::EVENT_ID, $id);
    }

    /**
     * Set Base Total Amount
     *
     * @param int $total
     * @return EventInterface
     */
    public function setBaseTotalAmount($total)
    {
        return $this->setData(self::TOTAL_AMOUNT, $total);
    }

    /**
     * Set Comment
     *
     * @param string $content
     * @return EventInterface
     */
    public function setComment($content)
    {
        return $this->setData(self::COMMENT, $content);
    }

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return EventInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set updated at.
     *
     * @param string $updatedAt
     * @return EventInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Set Order ID
     *
     * @param int $orderId
     * @return EventInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Set Visibility
     *
     * @param $visibility
     * @return EventInterface
     */
    public function setVisibility($visibility)
    {
        return $this->setData(self::VISIBILITY, $visibility);
    }
}
