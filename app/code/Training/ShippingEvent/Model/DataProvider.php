<?php
namespace Training\ShippingEvent\Model;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;

    protected $loadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Training\ShippingEvent\Model\ResourceModel\Event\CollectionFactory $shippingEventCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $shippingEventCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $shippingEventItem) {
            $this->loadedData[$shippingEventItem->getId()] = $shippingEventItem->getData();
        }
        return $this->loadedData;
    }
}
