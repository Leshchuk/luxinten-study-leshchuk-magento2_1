<?php
namespace Training\ShippingEvent\Model\Event\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = [
            \Training\ShippingEvent\Model\Event::STATUS_DISABLED => __('Invisible'),
            \Training\ShippingEvent\Model\Event::STATUS_ENABLED => __('Visible'),
        ];

        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
