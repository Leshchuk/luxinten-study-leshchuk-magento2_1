/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'jquery',
    'mage/mage',
    'mage/decorate'
], function (Component, customerData, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Training_ShippingEvent/freeshipping'
        },
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.freeshippingData = customerData.get('freeshipping');
            this.has_freeshipping = 1;
            this.freeshipping = {available: 1};
        }
    });
});
