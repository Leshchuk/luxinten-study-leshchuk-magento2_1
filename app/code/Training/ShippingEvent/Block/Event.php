<?php

namespace Training\ShippingEvent\Block;

class Event extends \Magento\Framework\View\Element\Template
{
    const XML_PATH_MINSHIPPING_AMOUNT = 'shipping_settings/general/minimum_shipping_amount';

    public function getFreeShippingAmount()
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_MINSHIPPING_AMOUNT);
    }

}
