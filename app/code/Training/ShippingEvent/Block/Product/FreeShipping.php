<?php

namespace Training\ShippingEvent\Block\Product;

class FreeShipping extends \Magento\Catalog\Block\Product\View
{
    const XML_PATH_MINSHIPPING_AMOUNT = 'shipping_settings/general/minimum_shipping_amount';

    public function canShow()
    {
        $freeShippingAmount = $this->_scopeConfig->getValue(self::XML_PATH_MINSHIPPING_AMOUNT);
        $productPrice       = $this->getProduct()->getFinalPrice();
        return $productPrice >= $freeShippingAmount;
    }
}
