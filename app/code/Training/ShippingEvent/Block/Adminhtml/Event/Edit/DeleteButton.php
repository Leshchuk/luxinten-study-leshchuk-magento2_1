<?php
namespace Training\ShippingEvent\Block\Adminhtml\Event\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class DeleteButton extends \Training\ShippingEvent\Block\Adminhtml\Event\Edit\CreateButton implements ButtonProviderInterface
{
    public function getButtonData()
    {
        $data = [];
        if ($this->getEventId()) {
            $data = [
                'label' => __('Delete'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure delete this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
                'sort_order' => 20,
            ];
        }
        return $data;
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['event_id' => $this->getEventId()]);
    }
}
