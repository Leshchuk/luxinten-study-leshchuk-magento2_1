<?php
namespace Training\ShippingEvent\Block\Adminhtml\Event\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class CreateButton implements ButtonProviderInterface
{
    protected $context;

    protected $shippingEventFactory;

    public function __construct(
        Context $context,
        \Training\ShippingEvent\Model\EventFactory $shippingEventFactory
    ) {
        $this->context = $context;
        $this->shippingEventFactory = $shippingEventFactory;
    }

    public function getEventId()
    {
        try {
            $shippingEventId = $this->context->getRequest()->getParam('event_id');
            return $this->shippingEventFactory->create()->load($shippingEventId)->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

    public function getButtonData()
    {

    }
}